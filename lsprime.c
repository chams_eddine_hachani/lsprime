#include <stdio.h>
#include <stdlib.h>
#include <math.h> // Required for sqrt()

#include "lsprime.h"


void initArray(int *tab, int len){

    if(!tab){
        printf("NULL ptr sent\n");
        return EXIT_FAILURE;
    }

    tab[0] = tab[1] = 0;
    for (int i = 2; i < len; i++){
        tab[i] = 1;
    }
}


void seiveEratos(int low, int high){
    /* You can learn more about this algorithm here :
       https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
    */

    int arr[high];
    int i = 2, j, k, sq;

    initArray(arr,high);
    
    sq = sqrt(high);

    for(i;i<sq;i++){
        j = 0;
        k = 0;
        if(arr[i] == 1){
            while(j<high){
                arr[j] = 0;
                j = pow(i,2) + k*i;
                k++;
            }
        }
    }

    // Output
    for(i=low;i<high;i++){
        if(arr[i] == 1){
            printf("%d  ",i);
        }
    }
    printf("\n");

}


void personalAlg(int low, int high){
    int i,j,k=0;
    int arr[high];

    for(i=low; i<high; i++){
        for(j=2; j<i/2; j++){ // The counter stops at j/2 because no number can be devided by a number bigger than its half except itself
            if(i % j == 0)
                break;
        }
        if(i % j != 0){
            arr[k] = i;
            k++;
        }
    }

    // Output
    for(i=0; i<k; i++){
        printf("%d  ",arr[i]);
    }
    printf("\n");

}

