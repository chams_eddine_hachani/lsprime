#ifndef LSPRIME_H
#define LSPRIME_H

void seiveEratos(int low, int high);
void initArray(int *tab, int len);
void personalAlg(int low, int high);

#endif // LSPRIME_H
