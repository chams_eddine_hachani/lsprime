#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


void usage(){
    printf("Usage lsprime [option] <arg1> <arg2>\n");
    printf("Options can be -s or -p which correspond to two different algorithms\n");
    printf("You can not use both\n");
}


void swap(int *a, int *b){
    if(!a || !b){
        printf("NULL pointer sent\n");
        return EXIT_FAILURE;
    }
    
    int temp = *a;
    *a = *b;
    *b = temp;
}


int main(int argc, char *argv[]) {
    if(argc != 4){
        usage();
        return EXIT_FAILURE;
    }

    printf("Prime Number Lister :\n");
    int lowBound = atoi(argv[2]), highBound = atoi(argv[3]);
    if(lowBound > highBound)
        swap(&lowBound,&highBound);

    int opt;
    int sflag=0, pflag=0; // these are to make sure the user won't send both options

    while((opt = getopt(argc, argv, "sp")) != -1){
        switch(opt){
        case 's' :
            if(sflag){
                usage();
                exit(EXIT_FAILURE);
            }
            sflag++;
            pflag++;
            seiveEratos(lowBound,highBound);
            break;
        case 'p' :
            if(pflag){
                usage();
                exit(EXIT_FAILURE);
            }
            sflag++;
            pflag++;
            personalAlg(lowBound, highBound);
            break;
        }
    }



    return 0;
}
